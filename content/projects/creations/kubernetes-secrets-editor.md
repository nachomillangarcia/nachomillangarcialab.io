{
    "title":"Kubernetes Web Secret Editor",
    "link":"https://github.com/bq/k8s-secret-editor",
    "image":"/img/k8s-secret-editor.jpg",
    "description":"Web interface to view, edit and backup secrets in Kubernetes.",
    "featured":true,
    "tags":["Kubernetes","Python","Flask","REST APIs"],
    "fact":"Inital commit and interface improvements",
    "sitemap": {"priority" : "0.8"}
}

Edit secrets in Kubernetes was a painful operations. Moreover, most of time this secrets contains configurations that needed to be edited by developers who didn't have knowledge about Kubernetes. To ease that tasks, we created this web interface to decode and edit secrets, and even storing them in a git repository as a backup.
