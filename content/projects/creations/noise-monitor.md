{
  "title": "Web Noise Monitor",
  "date": "2016-07-01T12:00:00-02:00",
  "link": "http://cecorsl.com/noise-monitor/sonometers/1",
  "image": "/img/cecor.jpg",
  "description": "Web platform to monitor noise levels, provided by wireless sonometers",
  "tags": ["IoT", "Ruby on Rails", "JavaScript", "D3js"],
  "featured":true
}

As my undergraduate thesis, I developed, in  collaboration with CECOR SL a platform to store and show in real time metrics from wireless sonometers deployed in festivals, constructions, etc. The backend is made with Ruby on Rails and the frontend in vanilla JavaScript and HTML. The dynamic charts and animations are made using the framework D3js.
