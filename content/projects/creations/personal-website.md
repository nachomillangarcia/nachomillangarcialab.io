{
  "title": "Personal webpage with CI/CD",
  "date": "2018-05-15T12:41:05-05:00",
  "image": "/img/personal-page.jpg",
  "link": "https://gitlab.com/nachomillangarcia/nachomillangarcia.gitlab.io",
  "description": "Personal website with CI/CD process in Gitlab for automatic updates",
  "tags": ["DevOps", "GitLab", "CI/CD", "Hugo", "CSS", "HTML"],
  "featured":true
}

This webpage  "<em>is based on Hugo engine </em>" and a template for personal portfolios.

It deploys changes automatically after perform tests and build. The solution is made with GitLab CI, using Kubernetes runners for the workload, and it deploys to GitLab Pages.
