{
  "title": "Prometheus Exporter",
  "date": "2018-06-02T12:00:00-02:00",
  "image": "/img/prometheus-exporter.jpg",
  "link": "https://github.com/nachomillangarcia/prometheus_aws_cost_exporter",
  "description": "Prometheus exporter to monitor AWS daily costs",
  "tags": ["Prometheus", "AWS", "Python", "Flask", "Helm"],
  "featured":true
}

Prometheus is a great system to monitor any metric in your platform, it implements exporters that collect and expose those metrics. I coded one that works with AWS Cost Explorer API to expose daily costs of any account and let monitor them with Prometheus. Then you can configure alerts, charts, etc.
