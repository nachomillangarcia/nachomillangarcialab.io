{
  "title": "Helm Charts",
  "date": "2018-05-30T12:00:00-02:00",
  "link": "https://github.com/nachomillangarcia?utf8=%E2%9C%93&tab=repositories&q=helm&type=&language=",
  "image": "/img/helm.png",
  "description": "Helm charts to share and distribute apps for Kubernetes",
  "tags": ["Helm", "Kubernetes", "DevOps"],
  "featured":true
}

Helm is a standard framework to create, share and deploy Kubernetes apps in the most easy way. I am used to make helm charts for every software I deploy in Kubernetes, so I can share with the community and use my work in any cluster.
