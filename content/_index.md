---
title: "Home"
date: 2018-02-10T18:56:13-05:00
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
<p>DevOps Architect with broad experience in programming solid, efficient infrastructures and development pipelines. </p>

<p>I enable organizations to deliver faster, higher quality and more secure software.</p>
